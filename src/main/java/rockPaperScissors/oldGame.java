package rockPaperScissors;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class oldGame {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new oldGame().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) { 

            // Initial message and round count
            System.out.println("Let's play round " + roundCounter);

            // Retrieving a random item from the list of possible choices
            Random randomizer = new Random();
            String random = rpsChoices.get(randomizer.nextInt(rpsChoices.size()));

            // Input from human, prompt ask to choose move. Enter while loop to check if choice is legal
            String humanMove = readInput("Your choice (Rock/Paper/Scissors)?");
            while (rpsChoices.contains(humanMove) != true) { // Cheking if move is in list of moves
                humanMove = readInput("I do not understand " + humanMove + ". Could you try again?");
            }

            // Defining the rules and deciding the score of a single round
            if (humanMove.equals(random)) {
                roundCounter += 1;
                humanScore += 0;
                computerScore += 0;
            }

            else if (humanMove.equals("paper") && random.equals("rock")) {
                roundCounter += 1;
                humanScore += 1;
            }

            else if (humanMove.equals("rock") && random.equals("scissors")) {
                roundCounter += 1;
                humanScore += 1;
            }
            
            else if (humanMove.equals("scissors") && random.equals("paper")) {
                roundCounter += 1;
                humanScore += 1;
            } else {
                roundCounter += 1;
                computerScore += 1;
            }

            // Printing what the human and computer chose
            System.out.print("Human chose " + humanMove + ", computer chose " + random + ". ");

            // Using the winLose function to print who wins or if its a tie
            System.out.println(winLose(humanMove, random));
            
            // Printing the score
            System.out.println("Score: human " + humanScore + " computer " + computerScore);
            
            // Aksing if the player want to play again, any other response than "n" results in a new round
            String playMore = readInput("Do you wish to continue playing? (y/n)?");
            if (playMore.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        }    
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    // Function decides the result of a game. Could have done the same for the score
    public String winLose(String user, String computer) {
        String computerWins = "Computer wins!";
        String humanWins = "Human wins!";
        String tieWins = "Its a tie!";

        if (computer.equals(user)) {
            return tieWins;
        }

        if (user.equals("rock")) {
            if (computer.equals("scissors")) {
                return humanWins;
            }
            else {
                return computerWins;
            }
        }
        
        else if (user.equals("paper")) {
            if (computer.equals("scissors")) {
                return computerWins;
            }
            else {
                return humanWins;
            }
        }
        
        else {
            if (computer.equals("paper")) {
                return humanWins;
            }
            else {
                return computerWins;
            }
        }
    }
}
