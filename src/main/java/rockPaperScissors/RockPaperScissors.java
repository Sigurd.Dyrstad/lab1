package rockPaperScissors;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) { 

            // Initial message and round count, adding another round for the next iteration
            System.out.println("Let's play round " + roundCounter);
            roundCounter += 1;

            // Retrieving a random item from the list of possible choices
            Random randomizer = new Random();
            String random = rpsChoices.get(randomizer.nextInt(rpsChoices.size()));

            // Input from human, prompt ask to choose move. Enter while loop to check if choice is legal
            String humanMove = readInput("Your choice (Rock/Paper/Scissors)?");
            while (rpsChoices.contains(humanMove) != true) { // Cheking if move is in list of moves
                humanMove = readInput("I do not understand " + humanMove + ". Could you try again?");
            }

            // Printing what the human and computer chose
            System.out.print("Human chose " + humanMove + ", computer chose " + random + ". ");
        
            // Defining the rules and deciding the score of a single round
            if (humanMove.equals(random)) {
                System.out.println("Its a tie!");
            }

            else if ((humanMove.equals("paper") && random.equals("rock")) 
            || (humanMove.equals("rock") && random.equals("scissors")) 
            || (humanMove.equals("scissors") && random.equals("paper"))) {
                
                humanScore += 1;
                System.out.println("Human wins!");
            } else {
                computerScore += 1;
                System.out.println("Computer wins!");  
            }

            // Printing the score
            System.out.println("Score: human " + humanScore + " computer " + computerScore);
            
            // Aksing if the player want to play again, any other response than "n" results in a new round
            String playMore = readInput("Do you wish to continue playing? (y/n)?");
            if (playMore.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        }    
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}